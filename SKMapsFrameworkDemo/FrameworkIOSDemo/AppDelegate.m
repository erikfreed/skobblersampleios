//
//  AppDelegate.m
//  FrameworkIOSDemo
//
//  Copyright (c) 2015 Skobbler. All rights reserved.
//

#import "AppDelegate.h"
#import <Foundation/Foundation.h>
#import <SKMaps/SKMaps.h>
#import "XMLParser.h"

#import <SKTDownloadAPI.h>

@interface AppDelegate ()<SKMapVersioningDelegate>

@end

@implementation AppDelegate

static NSString* const API_KEY = @"myapikey";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    if (!API_KEY || [API_KEY isEqualToString:@""] || [API_KEY stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Init error" message:@"API_KEY not set" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        
        return YES;
    }
    
    SKMapsInitSettings* initSettings = [[SKMapsInitSettings alloc]init];
    initSettings.mapDetailLevel = SKMapDetailLevelFull; //Use Full version of maps.
                                                        //Can be set to a light version.
    
    [[SKMapsService sharedInstance]initializeSKMapsWithAPIKey:API_KEY settings:initSettings];
    [[SKPositionerService sharedInstance]startLocationUpdate];
    [SKMapsService sharedInstance].mapsVersioningManager.delegate= self;
    
    [SKTDownloadManager sharedInstance];
    
    self.rootViewController = [[RootViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:self.rootViewController];
    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    navigationController.navigationBar.translucent = NO;
    self.window.rootViewController = navigationController;
    [self.window makeKeyAndVisible];
    
    self.cachedMapRegions = [NSMutableArray array];

    return YES;
}

- (void)mapsVersioningManager:(SKMapsVersioningManager *)versioningManager loadedWithMapVersion:(NSString *)currentMapVersion
{
    NSLog(@"Map version file download finished.\n");
    //needs to be updated for a new map version
    [[XMLParser sharedInstance] downloadAndParseJSON];
}

- (void)mapsVersioningManager:(SKMapsVersioningManager *)versioningManager loadedWithOfflinePackages:(NSArray *)packages updatablePackages:(NSArray *)updatablePackages
{
    NSLog(@"%lu updatable packages",(unsigned long)updatablePackages.count);
    for (SKMapPackage *package in updatablePackages)
    {
        NSLog(@"%@",package.name);
    }
}

- (void)mapsVersioningManager:(SKMapsVersioningManager *)versioningManager detectedNewAvailableMapVersion:(NSString *)latestMapVersion currentMapVersion:(NSString *)currentMapVersion
{
    NSLog(@"Current map version: %@ \n Latest map version: %@",currentMapVersion, latestMapVersion);
    
    NSString* message = [NSString stringWithFormat:@"A new map version is available on the server: %@ \n Current map version: %@",latestMapVersion,currentMapVersion];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New map version available" message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Update", nil];
        [alert show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSArray *availableVersions = [[SKMapsService sharedInstance].mapsVersioningManager availableMapVersions];
        SKVersionInformation *latestVersion = availableVersions[0];
        [[SKMapsService sharedInstance].mapsVersioningManager updateToVersion:latestVersion.version];
    }
}


@end
