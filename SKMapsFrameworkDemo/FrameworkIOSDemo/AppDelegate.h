//
//  AppDelegate.h
//  FrameworkIOSDemo
//
//  Copyright (c) 2015 Skobbler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@class SKTMapsObject;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) RootViewController *rootViewController;

@property(nonatomic,strong) NSMutableArray *cachedMapRegions;
@property (nonatomic, strong) SKTMapsObject *skMapsObject;

@end
