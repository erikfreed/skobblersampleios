//
//  RootViewController.m
//  FrameworkIOSDemo
//
//  Copyright (c) 2015 Skobbler. All rights reserved.
//

#import "RootViewController.h"
#import "MapCreatorViewController.h"
#import "MapStylesViewController.h"
#import "RoutingViewController.h"
#import "AnnotationsViewController.h"
#import "MapXMLViewController.h"
#import "MapDownloadViewController.h"
#import "NearbySearchViewController.h"
#import "ReverseGeocodeViewController.h"
#import "OverlaysViewController.h"
#import "RealReachViewController.h"
#import "AlternativeRoutesViewController.h"
#import "POITrackerViewController.h"
#import "CategorySearchViewController.h"
#import "MultiStepSearchViewController.h"
#import "GPSFilesViewController.h"
#import "NavigationUIViewController.h"
#import "HeatMapSettingsViewController.h"
#import "MapJSONViewController.h"
#import "AppDelegate.h"
#import "SKTMapsObject.h"
#import "TracksViewController.h"
#import "MapDisplayIBViewController.h"
#import "PedestrianNavigationViewController.h"
#include "TestingRootViewController.h"

@interface RootViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *dataSource;
@end

@implementation RootViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.title = @"SKMaps Demo";
    
	UITableView *featuresTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
	featuresTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	featuresTableView.delegate = self;
	featuresTableView.dataSource = self;
	[self.view addSubview:featuresTableView];
    
	self.dataSource = @[@[@"Map display", @"Map creator", @"Map styles", @"Annotations", @"Overlays", @"HeatMaps",@"Map JSON & download"],
                        @[@"Routing & Navigation", @"Alternative routes", @"Real reach", @"Tracks", @"POI tracking", @"Car navigation UI", @"Pedestrian navigation UI"],
	                    @[@"Address search - offline", @"Category search", @"Nearby search", @"Offline reverse geocoding"],
                        @[@"Features"]
                        ];
}

#pragma mark - UITableView delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int numberOfRows = (int)[[self.dataSource objectAtIndex:section] count];
	return numberOfRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *sectionTitle = @"";
	switch (section) {
		case 0:
			sectionTitle = @"Map";
			break;
            
		case 1:
			sectionTitle = @"Navigation";
			break;
            
		case 2:
			sectionTitle = @"Searches";
			break;
            
        case 3:
            sectionTitle = @"Testing";
            break;
            
		default:
			break;
	}
	return sectionTitle;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
	if (cell == nil)
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
	cell.textLabel.text = [[self.dataSource objectAtIndex:[indexPath section]] objectAtIndex:[indexPath row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	switch ([indexPath section]) {
		case 0: //Map
		{
			switch ([indexPath row]) {
                case 0:
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MapDisplayIBViewController" bundle:nil];
                    MapDisplayIBViewController *simpleMapDisplayVC = (MapDisplayIBViewController*)[storyboard instantiateViewControllerWithIdentifier:@"MapDisplayIBViewController"];
                    [self.navigationController pushViewController:simpleMapDisplayVC animated:YES];
                    break;
                }
                case 1:
				{
					MapCreatorViewController *mapCreatorVC = [[MapCreatorViewController alloc]init];
					[self.navigationController pushViewController:mapCreatorVC animated:YES];
					break;
				}
                    
				case 2:
				{
					MapStylesViewController *mapStylesVC = [[MapStylesViewController alloc]init];
					[self.navigationController pushViewController:mapStylesVC animated:YES];
					break;
				}
                    
				case 3:
				{
					AnnotationsViewController *annotationsVC = [[AnnotationsViewController alloc]init];
					[self.navigationController pushViewController:annotationsVC animated:YES];
					break;
				}
                    
				case 4:
				{
					OverlaysViewController *overlaysVC = [[OverlaysViewController alloc]init];
					[self.navigationController pushViewController:overlaysVC animated:YES];
					break;
				}
                    
				case 5:
				{
					HeatMapSettingsViewController *heatVC = [[HeatMapSettingsViewController alloc] init];
					[self.navigationController pushViewController:heatVC animated:YES];
					break;
				}
                case 6:
                {
                    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
                    NSArray *packages = [appDelegate.skMapsObject packagesForType:SKTPackageTypeContinent];
                    MapJSONViewController *mapXMLVC = [[MapJSONViewController alloc]initWithNibName:@"MapJSONViewController" bundle:nil withSKMapPackages:packages];
                    [self.navigationController pushViewController:mapXMLVC animated:YES];
                    break;
                }
				default:
					break;
			}
		}
			return;
            
		case 1: //Navigation
		{
			switch ([indexPath row]) {
				case 0:
				{
					RoutingViewController *routingVC = [[RoutingViewController alloc]init];
					[self.navigationController pushViewController:routingVC animated:YES];
					break;
				}
                    
				case 1:
				{
					AlternativeRoutesViewController *altRouteVC = [[AlternativeRoutesViewController alloc]init];
					[self.navigationController pushViewController:altRouteVC animated:YES];
					break;
				}
                    
				case 2:
				{
					RealReachViewController *realReachVC = [[RealReachViewController alloc]init];
					[self.navigationController pushViewController:realReachVC animated:YES];
					break;
				}
                    
				case 3:
				{
//					GPSFilesViewController *tracksVC = [[GPSFilesViewController alloc]init];
                    TracksViewController *tracksVC = [[TracksViewController alloc]init];
					[self.navigationController pushViewController:tracksVC animated:YES];
					break;
				}
                    
				case 4:
				{
					POITrackerViewController *poiTrackerVC = [[POITrackerViewController alloc]init];
					[self.navigationController pushViewController:poiTrackerVC animated:YES];
					break;
				}
                    
                    //Navigation UI
				case 5:
				{
					NavigationUIViewController *navUIVC = [[NavigationUIViewController alloc] init];
					[self.navigationController pushViewController:navUIVC animated:YES];
					break;
				}
                    
                case 6:
                {
                    PedestrianNavigationViewController *pedestrianNavVC = [[PedestrianNavigationViewController alloc] init];
                    [self.navigationController pushViewController:pedestrianNavVC animated:YES];
                    break;
                }
                    
				default:
					break;
			}
		}
			return;
            
		case 2: //Searches
		{
			switch ([indexPath row]) {
				case 0:
				{
					MultiStepSearchViewController *multiStepSearchVC = [[MultiStepSearchViewController alloc] initWithNibName:@"MultiStepSearchViewController" bundle:nil];
					multiStepSearchVC.dataSource = [[SKMapsService sharedInstance].packagesManager installedOfflineMapPackages];
					multiStepSearchVC.multiStepObject.listLevel = SKCountryList;
					[self.navigationController pushViewController:multiStepSearchVC animated:YES];
					break;
				}
                    
				case 1:
				{
					CategorySearchViewController *categorySearchVC = [[CategorySearchViewController alloc]initWithNibName:@"CategorySearchViewController" bundle:nil];
					[self.navigationController pushViewController:categorySearchVC animated:YES];
					break;
				}
                    
				case 2:
				{
					NearbySearchViewController *localSearchVC = [[NearbySearchViewController alloc]initWithNibName:@"NearbySearchViewController" bundle:nil];
					[self.navigationController pushViewController:localSearchVC animated:YES];
					break;
				}
                    
				case 3:
				{
					ReverseGeocodeViewController *reverseGeocodeVC = [[ReverseGeocodeViewController alloc]initWithNibName:@"ReverseGeocodeViewController" bundle:nil];
					[self.navigationController pushViewController:reverseGeocodeVC animated:YES];
					break;
				}
                    
				default:
					break;
			}
		}
            break;
            
        case 3: //Testing
        {
            TestingRootViewController *testingVC = [[TestingRootViewController alloc] init];
            [self.navigationController pushViewController:testingVC animated:YES];
        }
            break;
            
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
