//
//  GPSFilesViewController.m
//  FrameworkIOSDemo
//
//  Copyright (c) 2015 Skobbler. All rights reserved.
//

#import "GPSFilesViewController.h"
#import "GPSElementsDrawingViewController.h"
#import <CoreLocation/CLLocation.h>
#import <SKMaps/SKGPSFileElement.h>
#import <SKMaps/SKGPSFilesService.h>

@interface GPSFilesViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UITableView *tracksTableView;
@property(nonatomic,strong) NSArray* datasource;
@property(nonatomic,assign) GPSFileElementType type;

@property(nonatomic,strong) NSString *fileName;
@end

@implementation GPSFilesViewController

- (id)initWithType:(GPSFileElementType)type andDatasource:(NSArray*)datasource
{
    self = [super init];
    if (self) {
        self.datasource = datasource;
        self.type = type;
    }
    return self;
}

- (id)initWithFileName:(NSString*)fileName
{
    self = [super init];
    if (self) {
        self.fileName = fileName;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self populateTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) populateTable
{
    if (!self.datasource)
    {
        self.datasource = @[];
        NSString* path = [[NSBundle mainBundle] pathForResource:self.fileName ofType:@"gpx"];
        
        SKGPSFileElement* root = [[SKGPSFilesService sharedInstance] loadFileAtPath:path error:nil];
        self.datasource = [[SKGPSFilesService sharedInstance] childElementsForElement:root error:nil];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datasource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    cell.accessoryView = nil;
    
    if (self.type != GPSFileElementPoints)
    {
        SKGPSFileElement* gpsElement = self.datasource[indexPath.row];
        cell.textLabel.text = gpsElement.name;
        cell.detailTextLabel.text = [self stringForType:gpsElement.type];
        
        if (gpsElement.type == SKGPSFileElementGPXTrackSegment || gpsElement.type == SKGPSFileElementGPXRoute || gpsElement.type ==SKGPSFileElementGPXTrack)
        {
            UIButton* renderButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [renderButton setTitle:@"Draw" forState:UIControlStateNormal];
            [renderButton addTarget:self action:@selector(drawGPSCollection:event:) forControlEvents:UIControlEventTouchUpInside];
            renderButton.frame = CGRectMake(0.0f, 0.0f, 50.0f, 30.0f);;
            cell.accessoryView = renderButton;
        }
    }
    else
    {
        CLLocation* point = self.datasource[indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"(%.4f,%.4f)",point.coordinate.latitude, point.coordinate.longitude];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.type != GPSFileElementPoints)
    {
        SKGPSFileElement* gpsElement = self.datasource[indexPath.row];
        
        if (self.type == GPSFileElementCollection && gpsElement.type == SKGPSFileElementGPXTrack)
        {
            NSArray* children = [[SKGPSFilesService sharedInstance] childElementsForElement:gpsElement error:nil];
            GPSFilesViewController* subList = [[GPSFilesViewController alloc] initWithType:GPSFileElementSubcollection andDatasource:children];
            [self.navigationController pushViewController:subList animated:YES];
        }
        else
        {
            NSArray* points = [[SKGPSFilesService sharedInstance] locationsForElement:gpsElement];
            GPSFilesViewController* pointsVC = [[GPSFilesViewController alloc] initWithType:GPSFileElementPoints andDatasource:points];
            [self.navigationController pushViewController:pointsVC animated:YES];
        }
        
    }
}

-(IBAction)drawGPSCollection:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tracksTableView];
    NSIndexPath *indexPath = [self.tracksTableView indexPathForRowAtPoint:currentTouchPosition];
    
    SKGPSFileElement* gpsElement = self.datasource[indexPath.row];
    GPSElementsDrawingViewController* gpsDrawVC = [[GPSElementsDrawingViewController alloc] initWitGPSElement:gpsElement];
    [self.navigationController pushViewController:gpsDrawVC animated:YES];
}

-(NSString*)stringForType:(SKGPSFileElementType)type
{
    NSString* typeString;
    switch (type) {
        case SKGPSFileElementGPXRoot:{
            typeString = @"Root";
            break;
        }
        case SKGPSFileElementGPXRoute:{
            typeString = @"Route";
            break;
        }
        case SKGPSFileElementGPXRoutePoint:{
            typeString = @"RoutePoint";
            break;
        }
        case SKGPSFileElementGPXTrack:{
            typeString = @"Track";
            break;
        }
        case SKGPSFileElementGPXTrackSegment:{
            typeString = @"TrackSegment";
            break;
        }
        case SKGPSFileElementGPXTrackPoint:{
            typeString = @"TrackPoint";
            break;
        }
        case SKGPSFileElementGPXWaypoint:{
            typeString = @"Waypoint";
            break;
        }
        default:
            break;
    }
    return typeString;
    
}
@end
