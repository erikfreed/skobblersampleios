//
//  GPSFilesViewController.h
//  FrameworkIOSDemo
//
//  Copyright (c) 2015 Skobbler. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GPSFileElementType)
{
  GPSFileElementCollection,
  GPSFileElementSubcollection,
  GPSFileElementPoints
};

@interface GPSFilesViewController : UIViewController

- (id)initWithFileName:(NSString*)fileName;
- (id)initWithType:(GPSFileElementType)type andDatasource:(NSArray*)datasource;

@end
